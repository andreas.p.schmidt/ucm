-- Table: datatypes

CREATE TABLE users
(
	userid INTEGER PRIMARY KEY
);


-- DROP TABLE datatypes;

CREATE TABLE datatypes
(
  typeid varchar(255) NOT NULL,
  CONSTRAINT pk_datatypes PRIMARY KEY (typeid)
) 
WITH OIDS;
ALTER TABLE datatypes OWNER TO aschmidt;


-- Table: datecontextfact

-- DROP TABLE datecontextfact;

CREATE TABLE datecontextfact
(
  id int4 NOT NULL,
  featureid int4 NOT NULL,
  userid varchar(255) NOT NULL,
  validfrom timestamp,
  validuntil timestamp,
  confidence float4 NOT NULL,
  op varchar(10) NOT NULL,
  value timestamp NOT NULL,
  source varchar(255),
  transactiontime timestamp NOT NULL,
  CONSTRAINT fk_datecontextfact_feature FOREIGN KEY (featureid)
      REFERENCES feature (featureid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH OIDS;
ALTER TABLE datecontextfact OWNER TO aschmidt;

-- Table: feature

-- DROP TABLE feature;

CREATE TABLE feature
(
  featureid int4 NOT NULL DEFAULT nextval('"Feature_featureID_seq"'::text),
  uri varchar(255) NOT NULL,
  "type" varchar(255),
  agingfunction int4,
  cardinality int4 NOT NULL,
  parent int4,
  CONSTRAINT pk_feature PRIMARY KEY (featureid),
  CONSTRAINT fk_feature_datatypes FOREIGN KEY ("type")
      REFERENCES datatypes (typeid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_feature_feature FOREIGN KEY (parent)
      REFERENCES feature (featureid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH OIDS;
ALTER TABLE feature OWNER TO aschmidt;


-- Index: "featureURIIndex"

-- DROP INDEX "featureURIIndex";

CREATE UNIQUE INDEX "featureURIIndex"
  ON feature
  USING btree
  (featureid);

-- Table: numericalcontextfact

-- DROP TABLE numericalcontextfact;

CREATE TABLE numericalcontextfact
(
  id int4 NOT NULL,
  featureid int4 NOT NULL,
  userid varchar(255) NOT NULL,
  validfrom timestamp,
  validuntil timestamp,
  confidence float4 NOT NULL,
  op varchar(10) NOT NULL,
  value varchar(255) NOT NULL
  source varchar(255) NOT NULL,
  transactiontime timestamp NOT NULL,
) 
WITH OIDS;
ALTER TABLE numericalcontextfact OWNER TO aschmidt;


-- Index: "confidenceIndex"

-- DROP INDEX "confidenceIndex";

CREATE INDEX "confidenceIndex"
  ON numericalcontextfact
  USING btree
  (confidence);

-- Index: "featureIDIndex"

-- DROP INDEX "featureIDIndex";

CREATE INDEX "featureIDIndex"
  ON numericalcontextfact
  USING btree
  (featureid);



-- Table: ontologycontextfact

-- DROP TABLE ontologycontextfact;

CREATE TABLE ontologycontextfact
(
  id int4 NOT NULL,
  featureid int4 NOT NULL,
  userid varchar(255) NOT NULL,
  validfrom timestamp,
  validuntil timestamp,
  confidence float4 NOT NULL,
  op varchar(10) NOT NULL,
  value varchar(255) NOT NULL,
  ontology varchar(255) NOT NULL,
  source varchar(255) NOT NULL,
  transactiontime timestamp NOT NULL,
  CONSTRAINT fk_ontologycontextfact_feature FOREIGN KEY (featureid)
      REFERENCES feature (featureid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH OIDS;
ALTER TABLE ontologycontextfact OWNER TO aschmidt;




-- Table: stringcontextfact

-- DROP TABLE stringcontextfact;

CREATE TABLE stringcontextfact
(
  id int4 NOT NULL,
  featureid int4 NOT NULL,
  userid varchar(255) NOT NULL,
  validfrom timestamp,
  validuntil timestamp,
  confidence float4 NOT NULL,
  op varchar(10) NOT NULL,
  value varchar(255) NOT NULL,
  source varchar(255) NOT NULL,
  transactiontime timestamp NOT NULL,
  CONSTRAINT fk_stringcontextfact_feature FOREIGN KEY (featureid)
      REFERENCES feature (featureid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH OIDS;
ALTER TABLE stringcontextfact OWNER TO aschmidt;



