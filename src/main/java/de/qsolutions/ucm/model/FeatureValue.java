package de.qsolutions.ucm.model;

import java.util.Date;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface FeatureValue
{
	public String getSubject();
	public Feature getFeature();
	
	public Date getValidFrom();
	public Date getValidUntil();
	public double getConfidence();
	
	public Object getValue();
}
