package de.qsolutions.ucm.model;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public abstract class FeatureInternalImpl extends AbstractFeature implements FeatureInternal
{
	protected String uri;
	protected Type  type;
	protected int cardinality;
	
	protected OntologyEntity domain;
	protected OntologyEntity range;

	public abstract AgingFunction getAgingFunction();

	public String getURI()
	{
		return uri;
	}

	public Type getType()
	{
		return type;
	}

	public int getCardinality()
	{
		return cardinality;
	}
	
	public OntologyEntity getDomainSpecification()
	{
		return domain;
	}

	public OntologyEntity getRangeSpecification()
	{
		return range;
	}
}