package de.qsolutions.ucm.model;

import java.util.Date;

import de.qsolutions.util.TimePeriod;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface ContextFact
{
	public FeatureInternal getFeature();
	public String getSubject();
	
	public TimePeriod getValidPeriod();
	
	public double getInitialConfidence();
	public double getCurrentConfidence();

	public Mode getMode();
	
	public Object getValue();

	public Date   getTransactionTime();

	public String getSource();

}

