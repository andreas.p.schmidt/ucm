package de.qsolutions.ucm.model;

import java.util.Date;

import de.qsolutions.util.TimePeriod;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class ContextFactImpl implements ContextFact
{
	protected FeatureInternal feature;
	protected String subject;
	protected TimePeriod validPeriod;
	protected double confidence;
	protected Object value;
	protected Date transactionTime;
	protected String source;
	protected Mode operator;
	protected double currentConfidence;

	public FeatureInternal getFeature()
	{
		return feature;
	}
	public String getSubject()
	{
		return subject;
	}
	
	public TimePeriod getValidPeriod()
	{
		return validPeriod;
	}
		
	public double getInitialConfidence()
	{
		return confidence;
	}
	
	public Object getValue()
	{
		return value;
	}
	
	public Date getTransactionTime()
	{
		return transactionTime;
	}
	
	public String getSource()
	{
		return source;
	}
	
	public Mode getMode()
	{
		return operator;
	}
	
	public double getCurrentConfidence()
	{
		return currentConfidence;
	}

	public double getConfidence()
	{
		return confidence;
	}

	public void setConfidence(double confidence)
	{
		this.confidence = confidence;
	}

	public void setCurrentConfidence(double currentConfidence)
	{
		this.currentConfidence = currentConfidence;
	}

	public void setFeature(FeatureInternal feature)
	{
		this.feature = feature;
	}

	public void setOperator(Mode operator)
	{
		this.operator = operator;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public void setTransactionTime(Date transactionTime)
	{
		this.transactionTime = transactionTime;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public void setValidPeriod(TimePeriod p)
	{
		this.validPeriod = p;
	}

	public void setValue(Object value)
	{
		this.value = value;
	}

}
