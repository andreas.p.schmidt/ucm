package de.qsolutions.ucm.model;

import java.util.List;
import java.util.Set;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface Feature 
{
	public String 	getURI();
	public Type 	getType();
	
	public OntologyEntity getRangeSpecification();
	public OntologyEntity getDomainSpecification();
	
	public int 		getCardinality();
	
	public List<Feature> getParents();
	public List<Feature> getAllParents();
	
	public Set<Feature> getTopmostParents();
	
	public boolean	 isSubFeatureOf(Feature f);
	public boolean isSubFeatureOfOrEqual(Feature f);
	public boolean isSuperFeatureOf(Feature f);
	public boolean isSuperFeatureOfOrEqual(Feature f);
}
