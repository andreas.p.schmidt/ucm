package de.qsolutions.ucm.model;

import java.util.Date;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class FeatureValueImpl implements FeatureValue
{
	protected Feature 	feature;
	protected String  	subject;
	protected Date	  	validFrom;
	protected Date    	validUntil;
	protected double  confidence;
	protected Object   value;
	
	public FeatureValueImpl(Feature feature, String subject, Date validFrom, Date validUntil, double confidence, Object value)
	{
		this.feature = feature;
		this.subject = subject;
		this.validFrom = validFrom;
		this.validUntil = validUntil;
		this.value = value;
	}
	
	public Feature getFeature() 
	{
		return feature;
	}

	public String getSubject() 
	{
		return subject;
	}

	public Date getValidFrom() 
	{
		return validFrom;
	}

	public Date getValidUntil() 
	{
		return validUntil;
	}

	public double getConfidence() 
	{
		return confidence;
	}

	public Object getValue() 
	{
		return value;
	}

}
