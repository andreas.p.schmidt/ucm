package de.qsolutions.ucm.model;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public abstract class AbstractFeature implements Feature
{
	public Set<Feature> getTopmostParents()
	{
		Set<Feature> features = new HashSet<Feature>();
		
		List<Feature> p = getAllParents();
				
		if (p.isEmpty())
		{
			features.add(this);
		}
		else
		{
		
			for (Feature cf : p)
			{
				if (cf.getParents().isEmpty())
					features.add(cf);
			}
		}
		
		return features;
				
	}
	
	public List<Feature> getAllParents()
	{
		List<Feature> result = new LinkedList<Feature>();
		LinkedList<Feature> todo = new LinkedList<Feature>();
		todo.addAll(getParents());
		
		while (! todo.isEmpty())
		{
			Feature f = todo.removeFirst();
			result.add(f);
			todo.addAll(f.getParents());
		}
		
		return result;
	}
	
	public boolean equals(Object o)
	{
		if (! (o instanceof Feature))
			return false;
		else
			return getURI().equals( ((Feature) o).getURI());
	}
	
	public int hashCode()
	{
		return getURI().hashCode();
	}
	

	public boolean isSubFeatureOf(Feature f)
	{
		return f.isSuperFeatureOf(this);
	}

	public boolean isSuperFeatureOf(Feature f)
	{
		Queue<Feature> todo = new LinkedList<Feature>();
		todo.addAll(getParents());
		
		while (!todo.isEmpty())
		{
			Feature x = todo.poll();
			
			if (x.equals(f))
				return true;
			
			todo.addAll(x.getParents());			
		}

		return false;
	}

	public boolean isSuperFeatureOfOrEqual(Feature f)
	{
		return equals(f) || isSuperFeatureOf(f);
	}

	public boolean isSubFeatureOfOrEqual(Feature f)
	{
		return equals(f) || isSubFeatureOf(f);
	}

}

