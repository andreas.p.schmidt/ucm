package de.qsolutions.ucm.model;


/**
 * @author aschmidt
 */
public interface FeatureInternal extends Feature
{
	public AgingFunction getAgingFunction();
}
