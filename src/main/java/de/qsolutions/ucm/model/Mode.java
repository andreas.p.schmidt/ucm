package de.qsolutions.ucm.model;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public enum Mode 
{
	POSITIVE,
	NEGATIVE
}
