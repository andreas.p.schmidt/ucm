package de.qsolutions.ucm.query;

public class NumberCondition extends AtomicCondition<Number, Number>
{
	public NumberCondition(NumberOperator op, Number entity)
	{
		super(op, entity);
	}
}
