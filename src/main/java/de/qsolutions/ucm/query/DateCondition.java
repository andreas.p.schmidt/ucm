package de.qsolutions.ucm.query;

import java.util.Date;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class DateCondition extends AtomicCondition<Date,Date[]>
{	
	public DateCondition(Operator<Date,Date[]> op, Date d)
	{
		this(op, new Date[] { d });
	}
	
	public DateCondition(Operator<Date,Date[]> op, Date[] d)
	{
		super(op,d);
	}
}
