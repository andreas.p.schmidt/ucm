package de.qsolutions.ucm.query;

public interface Operator<DataType, ArgType>
{
	boolean evaluate(DataType value, AtomicCondition<DataType,ArgType> condition);
}
