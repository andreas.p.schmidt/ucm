package de.qsolutions.ucm.query;


/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class StringCondition extends AtomicCondition<String,String> 
{
	public StringCondition(StringOperator op, String entity)
	{
		super(op,entity);
	}
}
