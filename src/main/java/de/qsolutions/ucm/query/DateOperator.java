package de.qsolutions.ucm.query;

import java.util.Date;

import de.qsolutions.util.TimePeriod;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public enum DateOperator implements Operator<Date,Date[]>
{
	EQUALS("=",1),
	LESS_OR_EQUAL("<=",1),
	GREATER_OR_EQUAL(">=",1),
	BETWEEN("BETWEEN",2);
	
	private String op;
	private int argCount;
	
	private DateOperator(String op, int i)
	{
		this.op = op;
		argCount = i;
	}
	
	public String operator()
	{
		return op;
	}
	
	public int argCount()
	{
		return argCount;
	}
	public String toString()
	{
		return op;
	}

	public boolean evaluate(Date value, AtomicCondition<Date,Date[]> c)
	{		
		if (c.getArg().length < argCount)
			throw new IllegalArgumentException("Arguments for operator " + op + " are not sufficient (required: " + argCount + ", actual: " + c.getArg().length);
		
		switch (this)
		{
			case EQUALS:
				return c.getArg()[0].equals(value);
				
			case LESS_OR_EQUAL:
				return c.getArg()[0].compareTo(value) >= 0;
				
			case GREATER_OR_EQUAL:
				return c.getArg()[0].compareTo(value) <= 0;
				
			case BETWEEN:
				return (new TimePeriod(c.getArg())).contains(value);
		}
		
		return false;
	}
	
}
