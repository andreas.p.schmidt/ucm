package de.qsolutions.ucm.query;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class AtomicCondition<DataType,ArgType> implements Condition
{
	protected ArgType arg;
	protected Operator<DataType,ArgType> op;	
	
	public AtomicCondition(Operator<DataType,ArgType> op, ArgType arg)
	{
		this.op = op;
		this.arg = arg;
	}
	
	public ArgType getArg()
	{
		return arg;
	}
	
	public void setArg(ArgType arg)
	{
		this.arg = arg;
	}
	
	public Operator<DataType,ArgType> getOp()
	{
		return op;
	}
	
	public void setOp(Operator<DataType,ArgType> op)
	{
		this.op = op;
	}
	
	public boolean evaluateValue(DataType value)
	{
		return getOp().evaluate(value, this);
	}
	
	@SuppressWarnings("unchecked")
	public boolean evaluate(Object value)
	{	 
		return evaluateValue((DataType) value);
	}
}
