package de.qsolutions.ucm.query;

public interface ConditionAtom extends Condition
{
	public boolean evaluate(Object value);
}
