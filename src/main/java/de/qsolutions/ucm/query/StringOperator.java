package de.qsolutions.ucm.query;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public enum StringOperator implements Operator<String,String>
{
	EQUALS("="),
	NOT_EQUALS("!=");
	
	private String op;
	
	private StringOperator(String op)
	{
		this.op = op;
	}
	
	public String operator()
	{
		return op;
	}
	
	public String toString()
	{
		return op;
	}

	public boolean evaluate(String value, AtomicCondition<String,String> c)
	{
		switch(this)
		{
			case EQUALS:
				return c.equals(value);
				
			case NOT_EQUALS:
				return ! c.equals(value);
		}
				
		return false;
	}
}
