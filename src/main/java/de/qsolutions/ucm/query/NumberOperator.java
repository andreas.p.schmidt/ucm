package de.qsolutions.ucm.query;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public enum NumberOperator implements Operator<Number,Number>
{
	EQUALS("="), 
	NOT_EQUALS("!="),
	GREATER_OR_EQUAL(">="),
	LESS_OR_EQUAL("<=");

	private String op;

	private NumberOperator(String op)
	{
		this.op = op;
	}

	public String operator()
	{
		return op;
	}
	public String toString()
	{
		return op;
	}

	public boolean evaluate(Number value, AtomicCondition<Number,Number> c)
	{
		switch (this)
		{
			case EQUALS:
				return c.getArg().equals(value);
			case NOT_EQUALS:
				return ! c.getArg().equals(value);
			case GREATER_OR_EQUAL:
				return c.getArg().doubleValue() <= value.doubleValue();			
			case LESS_OR_EQUAL:
				return c.getArg().doubleValue() >= value.doubleValue();
			
		}

		return false;
	}
}
