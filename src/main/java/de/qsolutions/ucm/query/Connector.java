package de.qsolutions.ucm.query;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public enum Connector
{
	AND,
	OR,
	NOT;
	
	public String toString()
	{
		if (this.equals(AND))
			return "AND";
		
		if (this.equals(OR))
			return "OR";
		
		if (this.equals(NOT))
			return "NOT";
		
		return "";
	}
}
