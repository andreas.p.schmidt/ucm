package de.qsolutions.ucm.provider.relational.postgres;

import java.util.Date;

import de.qsolutions.ucm.model.FeatureInternal;
import de.qsolutions.ucm.provider.relational.RelationalContextFact;

class PostgresContextFact extends RelationalContextFact
{
	public PostgresContextFact(FeatureInternal f)
	{
		super(f);
	}
	
	public PostgresContextFact(FeatureInternal f, 
			String userid, Date transactionTime,
			Date validFrom, Date validUntil,
			String op, String source, double confidence,
			Date current)
	{
		super(f,userid,transactionTime,validFrom,validUntil,op,source,confidence,current);
	}
	
}