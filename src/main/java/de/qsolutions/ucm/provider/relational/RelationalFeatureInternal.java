package de.qsolutions.ucm.provider.relational;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.qsolutions.ucm.aging.ExponentialAgingFunction;
import de.qsolutions.ucm.model.AgingFunction;
import de.qsolutions.ucm.model.Feature;
import de.qsolutions.ucm.model.FeatureInternalImpl;
import de.qsolutions.ucm.model.OntologyEntity;
import de.qsolutions.ucm.model.Type;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class RelationalFeatureInternal extends FeatureInternalImpl
{

	protected static Log log = LogFactory.getLog(RelationalFeatureInternal.class);

	protected static Map<String,Class<?>> AGING_FUNCTIONS = new HashMap<String,Class<?>>();
	
	static
	{
		AGING_FUNCTIONS.put("exp",ExponentialAgingFunction.class);
	}
	
	protected final RelationalSchemaManager manager;
	protected long featureID;
	protected AgingFunction agingFunction;
	
	public RelationalFeatureInternal(RelationalSchemaManager manager, ResultSet rs) throws SQLException
	{
		this.manager = manager;
		this.cardinality = rs.getInt("cardinality");
		this.featureID = rs.getLong("featureid");
		this.uri = rs.getString("uri");

		//TODO datatype representation has changed
		String t = rs.getString("type");
		this.type = Type.fromString(t);
	
		if (rs.getString("domain") != null)
			this.range = OntologyEntity.fromString(rs.getString("domain"));

		if (rs.getString("range") != null)
			this.range = OntologyEntity.fromString(rs.getString("rangw"));

		//TODO Check
		try
		{
			if (! AGING_FUNCTIONS.containsKey(rs.getString("agingfunction")))
				log.error("Aging function " + rs.getString("agingfunction") + " not found.");
			else
			{
				agingFunction = (AgingFunction) AGING_FUNCTIONS.get(rs.getString("agingfunction")).newInstance();
				
				double[] p = new double[3];
	
				p[0] = rs.getDouble("agingparam1");
				p[1] = rs.getDouble("agingparam2");
				p[2] = rs.getDouble("agingparam3");
				
				agingFunction.setParameters(p);
			}
		}
		catch (InstantiationException e)
		{
			log.error("",e);
		}
		catch (IllegalAccessException e)
		{
			log.error("",e);
		}
		catch (SQLException e)
		{
			log.error("",e);
		}
	}
	
	public long getFeatureID()
	{
		return featureID;
	}
	
	public void setFeatureID(long id)
	{
		featureID = id;
	}
	
	public RelationalContextFact createContextFact(ResultSet rs, Date current) throws SQLException
	{
		RelationalContextFact res = new RelationalContextFact(this);
		res.fromResultSet(rs,current);

		switch (type)
		{
			case ONTOLOGY:
				res.setValue(new OntologyEntity(rs.getString("value"), rs.getString("ontology")));
				break;
				
			case DATE:
				res.setValue(rs.getDate("value"));
				break;
				
			case STRING:
				res.setValue(rs.getString("value"));				
				break;
				
			case NUMBER:				
				res.setValue(new Double(rs.getDouble("value")));
				break;
		}

		return res;
	}

	public List<Feature> getParents()
	{
		return manager.getParentFeatures(this);
	}
	
	public AgingFunction getAgingFunction()
	{
		return agingFunction;
	}
}
