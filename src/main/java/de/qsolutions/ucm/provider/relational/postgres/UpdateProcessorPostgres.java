package de.qsolutions.ucm.provider.relational.postgres;

import java.sql.SQLException;

import de.qsolutions.ucm.provider.relational.RelationalContextStorage;

public class UpdateProcessorPostgres extends RelationalContextStorage
{
	
	protected void initialize() throws SQLException
	{		
		super.initialize();
		
		// use server-side prepared statements
		//((org.postgresql.PGStatement) insertOntology).setPrepareThreshold(0);
	}	
}
