package de.qsolutions.ucm.provider.relational;

import java.util.Date;
import java.util.Iterator;

import de.qsolutions.ucm.model.OntologyEntity;
import de.qsolutions.ucm.query.CompositeCondition;
import de.qsolutions.ucm.query.Condition;
import de.qsolutions.ucm.query.DateCondition;
import de.qsolutions.ucm.query.DateOperator;
import de.qsolutions.ucm.query.OntologyCondition;
import de.qsolutions.ucm.query.OntologyOperator;
import de.qsolutions.ucm.query.StringCondition;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class RelationalConditionEvaluator
{
	protected static final String TRUE = "TRUE";
	
	protected String formatString(String s)
	{
		return "'" + s + "'";	
	}
	
	protected String formatDate(Date d)
	{
		return (new java.sql.Date(d.getTime()).toString());
	}
	
	protected String formatOntologyEntity(String field, OntologyEntity e)
	{
		return "'" + e.getURI() + "' AND " + field + "ontology='" + e.getOntologyURI() + "')";
	}
	
	
	protected String getCompositeCondition(String field, CompositeCondition cc)
	{
		switch (cc.getConnector())
		{
			case AND:				
			case OR:
				return explode(field,cc);
				
			case NOT:
				return "NOT (" + getQueryCondition(field, cc.iterator().next()) +  ")";
		}
		
		return TRUE;
	}
	
	protected String getStringCondition(String field, StringCondition sc)
	{
		return field + " " + sc.getOp().toString() + " " + formatString(sc.getArg());		
	}
	
 	protected String getOntologyCondition(String field, OntologyCondition oc)
 	{
		OntologyOperator op = (OntologyOperator) oc.getOp();
		
		switch (op)
		{
			case EQUALS:
				OntologyEntity e = OntologyEntity.fromString(oc.getArg()[0]);				
				return "(" + field + oc.getOp().toString() + " " + formatOntologyEntity(field,e) + ")";
			
			case IN:
				StringBuffer b = new StringBuffer();
				b.append("(");
				
				for (int i = 0; i < oc.getArg().length; i++)
				{
					OntologyEntity e2 = OntologyEntity.fromString(oc.getArg()[i]);				
					b.append("(" + field + oc.getOp().toString() + " " + formatOntologyEntity(field,e2) + ")");
					
					if (i < oc.getArg().length - 1)
						b.append(" OR ");
					else
						b.append(")");
				}
				return b.toString();
		}
		
		return TRUE; 

 	}

 	protected String getDateCondition(String field, DateCondition dc)
 	{
		DateOperator op = (DateOperator) dc.getOp();
		
		switch (op)
		{
			case EQUALS:
			case GREATER_OR_EQUAL:
			case LESS_OR_EQUAL:
				return "value " + op.toString() + " " + formatDate(dc.getArg()[0]); 
		
			case BETWEEN:
				return field + " BETWEEN " 
					+ formatDate(dc.getArg()[0])  
					+ " AND " + formatDate(dc.getArg()[1]);
		}

		return TRUE;
 	}
 	
	public String getQueryCondition(String field, Condition c)
	{
		if (c instanceof CompositeCondition)
			return getCompositeCondition(field, (CompositeCondition) c);
		
		if (c instanceof StringCondition)
			return getStringCondition(field, (StringCondition) c);
		
		if (c instanceof OntologyCondition)
			return getOntologyCondition(field, (OntologyCondition) c);
		
		if (c instanceof DateCondition)
			return getDateCondition(field, (DateCondition) c);
		
		return "TRUE";
	}
	
	protected String explode(String field, CompositeCondition cc)
	{
		StringBuffer b = new StringBuffer();		
		Iterator<Condition> it =  cc.iterator();
		
		b.append("(");
		
		while (it.hasNext())
		{
			Condition c = it.next();
			
			b.append(getQueryCondition(field,c));
			
			if (it.hasNext())
				b.append(cc.getConnector().toString() + " ");
			else
				b.append(")");
			
		}
		return b.toString();
	}
	
}
