package de.qsolutions.ucm.aging;

import java.util.Calendar;
import java.util.Date;

import de.qsolutions.ucm.model.AgingFunction;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class ExponentialAgingFunction implements AgingFunction
{
	protected double r;
	protected double s;

	protected static final double DATE_SCALE_FACTOR = 60000;
	protected static final long DATE_ZERO;
	
	static
	{
		Calendar c = Calendar.getInstance();
		c.set(2008,1,1);
		DATE_ZERO = c.getTime().getTime();
	}
	
	public double getCurrentConfidence(Date initial, Date current,
			double initialConfidence)
	{
		double d1 = getDateValue(initial);
		double d2 = getDateValue(current);
		
		return Math.exp(r * (d2-d1) + s) * initialConfidence;		
	}

	public Date getDropoutDate(Date initial, double initialConfidence, double minimumConfidence)
	{
		double d = getDateValue(initial);
		double t = Math.log(minimumConfidence / initialConfidence) - s + r * d;
		double res = t / r;
		return getDate(res);
	}

	public double getTransformedInitialConfidence(Date t0, double alpha0)
	{
		double d = getDateValue(t0);
		return Math.exp(r*d + s) * alpha0;
	}

	public double getTransformedMinimumCondition(Date t, double alpha)
	{
		double d = getDateValue(t);
		return alpha / Math.exp(r * d);
	}
	
	protected double getDateValue(Date t)
	{
		double d = (t.getTime() - DATE_ZERO);
		return d / DATE_SCALE_FACTOR;
	}
	
	protected Date getDate(double dateValue)
	{
		double dt = dateValue * DATE_SCALE_FACTOR + DATE_ZERO;
		long dl = Math.round(dt);
		return new Date(dl);		
	}

	public void setParameters(double[] params)
	{
		r = params[0];
		s = params[1];
	}

}
