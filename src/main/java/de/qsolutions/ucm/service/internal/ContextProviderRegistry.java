package de.qsolutions.ucm.service.internal;

import java.util.List;

import de.qsolutions.ucm.service.provider.ContextProvider;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface ContextProviderRegistry 
{
	public void register(ContextProvider provider);
	public void unregister(String id);
	
	public List<ContextProvider> getProviders(String featureURI, String userID);
	public List<ContextProvider> getProviders(String[] featureURIs, String userID);

	public List<String> getSupportedUsers();
	public List<String> getSupportedFeatures();
}
