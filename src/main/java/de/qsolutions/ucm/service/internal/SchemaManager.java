package de.qsolutions.ucm.service.internal;

import java.util.List;

import de.qsolutions.ucm.model.Feature;
import de.qsolutions.ucm.model.Type;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface SchemaManager
{
	public List<Feature> getParentFeatures(Feature f);

	public Feature getFeatureForURI(String featureURI);

	public List<Feature> getRootFeatures();

	public List<Feature> getChildFeatures(Feature f);

	public List<Feature> getAllChildFeatures(Feature f);

	public List<Feature> getAllParentFeatures(Feature f);

	public boolean addFeature(String featureURI, Type type, int cardinality,
			List<String> parents, List<String> children, String agingFunction,
			double[] agingParams);

	public boolean addFeature(String featureURI, Type type, int cardinality,
			List<String> parents, List<String> children, String agingFunction,
			double[] agingParams, String domain, String range);

	public boolean deleteFeature(String featureURI);
}
