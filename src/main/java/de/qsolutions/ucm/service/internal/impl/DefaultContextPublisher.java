package de.qsolutions.ucm.service.internal.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import de.qsolutions.ucm.model.AgingFunction;
import de.qsolutions.ucm.model.ContextFact;
import de.qsolutions.ucm.model.Feature;
import de.qsolutions.ucm.service.internal.AnnotatedContextFact;
import de.qsolutions.ucm.service.internal.ContextFactPublisher;
import de.qsolutions.ucm.service.internal.ContextFactSubscriber;
import de.qsolutions.ucm.service.internal.ContextFactUpdateEvent;
import de.qsolutions.ucm.service.internal.ContextStorage;
import de.qsolutions.ucm.service.internal.ContextStorageListener;
import de.qsolutions.ucm.service.internal.FactQuery;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

@Component
public class DefaultContextPublisher implements ContextFactPublisher, ContextStorageListener
{
	protected Map<String,List<String>> subscriptions = 
		new HashMap<String,List<String>>();
	
	protected Map<String,ContextFactSubscriber> ids = new HashMap<String,ContextFactSubscriber>();	
	protected Map<String,FactQuery> queries = new HashMap<String,FactQuery>();
	
	protected long counter = 0;
	
	protected ContextStorage storage;
	protected FactEvaluator evaluator;

	public String subscribe(ContextFactSubscriber s, FactQuery q)
	{
		counter++;
		
		String id = Long.toString(counter);
		ids.put(id,s);
		queries.put(id,q);			

		for (String u : q.getFeatureURIs())
		{
			List<String> l = subscriptions.get(u);
		
			if (l == null)
			{
				l = new LinkedList<String>();
				subscriptions.put(u,l);			
			}
			
			l.add(id);					
		}		
		return id;
	}

	public void unsubscribe(String subscriptionID)
	{
		ContextFactSubscriber s = ids.get(subscriptionID);
		
		for (String f : subscriptions.keySet())
			subscriptions.get(f).remove(s);
	}
	
	public void setContextStorage(ContextStorage s)
	{
		if (storage != null)
			storage.removeStorageListener(this);
		
		storage = s;		
		storage.addStorageListener(this);		
	}
	
	public ContextStorage getContextStorage()
	{
		return storage;
	}

	public void factsAdded(List<ContextFact> facts)
	{		
		for (ContextFact f : facts)
		{
			AgingFunction af = f.getFeature().getAgingFunction();

			List<ContextFactSubscriber> cfs = getSubscribers(f);
		
			for (ContextFactSubscriber s : cfs)
			{
				AnnotatedContextFact a = new AnnotatedContextFact(f);
				
				if (af != null)
				{
					FactQuery q = queries.get(s.getID());
					double alpha = q.getMinimumCurrentConfidence();
					a.setDropOutDate(af.getDropoutDate(f.getTransactionTime(), f.getInitialConfidence(), alpha));
				}

				ContextFactUpdateEvent ev = new ContextFactUpdateEvent(Collections.singletonList(a));

				s.contextFactUpdate(ev);
			}
		}		
	}
	
	
	protected List<ContextFactSubscriber> getSubscribers(ContextFact fact)
	{		
		List<Feature> features = fact.getFeature().getAllParents();
		List<String> s = new LinkedList<String>();
		
		for (Feature f : features)
			s.addAll(subscriptions.get(f.getURI()));
			
		List<ContextFactSubscriber> res = new LinkedList<ContextFactSubscriber>();

		for (String id : s)
		{
			if (evaluator.matchesQuery(fact,queries.get(id)))
				res.add(ids.get(id));
		}
		
		return res;
	}

	public FactEvaluator getEvaluator()
	{
		return evaluator;
	}

	public void setEvaluator(FactEvaluator evaluator)
	{
		this.evaluator = evaluator;
	}
}
