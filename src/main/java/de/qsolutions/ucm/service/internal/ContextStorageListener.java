package de.qsolutions.ucm.service.internal;

import java.util.List;

import de.qsolutions.ucm.model.ContextFact;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface ContextStorageListener
{
	public void factsAdded(List<ContextFact> facts);
}
