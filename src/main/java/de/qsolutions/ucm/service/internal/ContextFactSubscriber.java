package de.qsolutions.ucm.service.internal;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface ContextFactSubscriber
{
	public String getID();
	public void contextFactUpdate(ContextFactUpdateEvent ev);
}
