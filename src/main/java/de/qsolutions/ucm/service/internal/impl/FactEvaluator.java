package de.qsolutions.ucm.service.internal.impl;

import org.springframework.stereotype.Component;

import de.qsolutions.ucm.model.ContextFact;
import de.qsolutions.ucm.model.Feature;
import de.qsolutions.ucm.model.Mode;
import de.qsolutions.ucm.query.CompositeCondition;
import de.qsolutions.ucm.query.Condition;
import de.qsolutions.ucm.query.ConditionAtom;
import de.qsolutions.ucm.query.QueryMode;
import de.qsolutions.ucm.service.internal.FactQuery;
import de.qsolutions.ucm.service.internal.SchemaManager;

@Component
public class FactEvaluator
{
	protected SchemaManager schemaManager;

	public FactEvaluator(SchemaManager sm)
	{
		this.schemaManager = sm;
	}
	
	public boolean matchesQuery(ContextFact f, FactQuery q)
	{
		if (q.getSubjectCondition() != null)
			return evaluateCondition(q.getSubjectCondition(),f.getSubject());

		if (q.getSource() != null)
		{
			if (! q.getSource().equals(f.getSource()))
				return false;
		}

		if (f.getCurrentConfidence() < q.getMinimumCurrentConfidence())
			return false;
		
		if (f.getInitialConfidence() < q.getMinimumInitialConfidence())
			return false;
		
		if (! q.getValidPeriod().contains(f.getValidPeriod()))
			return false;
		
		if (! q.getTransactionPeriod().contains(f.getTransactionTime()))
			return false;

		if (q.getMode() != QueryMode.BOTH)
		{
			switch (q.getMode())
			{
				case POSITIVE:
					return f.getMode() == Mode.POSITIVE;
				
				case NEGATIVE:
					return f.getMode() == Mode.NEGATIVE;
			}
		}

		// Feature
		if (! q.getFeatureURIs().equals(FactQuery.ALL_FEATURES))
		{
			Feature[] fqs = new Feature[q.getFeatureURIs().length];
			int i = 0;
			
			for (String s : q.getFeatureURIs())
				fqs[i++] = schemaManager.getFeatureForURI(s);
				
			boolean b = false;
			for (Feature ft : fqs)
			{
				if (ft.equals(f.getFeature()) || ft.isSubFeatureOf(f.getFeature()))
				{
					b = true;
					break;
				}
			}
			
			if (!b)
				return false;
		}

		if (q.getValueCondition() != null)
			return evaluateCondition(q.getValueCondition(),f.getValue());
		
				
		return true;
	}
	
	protected boolean evaluateCondition(Condition c, Object value)
	{
		if (c instanceof ConditionAtom)			
		{						
			return ((ConditionAtom) c).evaluate(value);
		}
		
		if (c instanceof CompositeCondition)
		{
			CompositeCondition cc = (CompositeCondition) c;

			switch (cc.getConnector())
			{
				case NOT:
					return ! evaluateCondition(cc.iterator().next(),value);
					
				case AND:
					for (Condition _c : cc)
					{
						if (! evaluateCondition(_c,value))
							return false;
					}
					return true;
					
				case OR:
					for (Condition _c : cc)
					{
						if (evaluateCondition(_c,value))
							return true;
					}
					return false;
			}			
		}		
		return false;
	}
}
