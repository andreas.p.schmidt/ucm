package de.qsolutions.ucm.service.internal;

import java.util.LinkedList;
import java.util.List;

import de.qsolutions.ucm.model.ContextFact;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public abstract class AbstractContextStorage implements ContextStorage
{
	protected List<ContextStorageListener> listeners = new LinkedList<ContextStorageListener>();
	
	public void addStorageListener(ContextStorageListener l)
	{
		listeners.add(l);
	}
	
	public void removeStorageListener(ContextStorageListener l)
	{
		listeners.remove(l);
	}
	
	protected void fireFactsAdded(List<ContextFact> facts)
	{		
		for (ContextStorageListener l : listeners)
			l.factsAdded(facts);
	}

}
