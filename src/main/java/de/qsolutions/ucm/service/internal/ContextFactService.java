package de.qsolutions.ucm.service.internal;

import java.util.List;

import de.qsolutions.ucm.model.ContextFact;

public interface ContextFactService
{

	public List<ContextFact> getFacts(FactQuery q);
	public void update(List<AddFeatureValue> updates);

	public ContextProviderRegistry getProviderRegistry();
	public void setProviderRegistry(
			ContextProviderRegistry providerRegistry);
	public SchemaManager getSchemaManager();
	public void setSchemaManager(SchemaManager schemaManager);
	public ContextStorage getStorage();
	public void setStorage(ContextStorage storage);
	public ContextFactPublisher getPublisher();
	public void setPublisher(ContextFactPublisher publisher);
}