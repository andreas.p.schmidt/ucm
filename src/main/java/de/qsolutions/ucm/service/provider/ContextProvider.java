package de.qsolutions.ucm.service.provider;

import java.util.Set;

public interface ContextProvider 
{
	public String getID();
	
	public Set<String>  getSupportedFeatures();
	public Set<String>  getSupportedSubjects();
}
