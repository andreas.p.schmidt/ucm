package de.qsolutions.ucm.service.provider;

import java.util.List;

import de.qsolutions.ucm.model.ContextFact;
import de.qsolutions.ucm.service.internal.FactQuery;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface PullContextProvider extends ContextProvider
{
	public List<ContextFact> getFacts(FactQuery q);

}
