package de.qsolutions.ucm.service.logical.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import de.qsolutions.ucm.model.ContextFact;
import de.qsolutions.ucm.model.FeatureValue;
import de.qsolutions.ucm.model.OntologyEntity;
import de.qsolutions.ucm.query.CompositeCondition;
import de.qsolutions.ucm.query.Condition;
import de.qsolutions.ucm.query.OntologyCondition;
import de.qsolutions.ucm.query.OntologyOperator;
import de.qsolutions.ucm.service.internal.ContextFactService;
import de.qsolutions.ucm.service.internal.FactQuery;
import de.qsolutions.ucm.service.logical.ConflictResolutionStrategy;
import de.qsolutions.ucm.service.logical.ContextQueryService;
import de.qsolutions.ucm.service.logical.FeatureValueCondition;
import de.qsolutions.ucm.service.logical.FeatureValueQuery;
import de.qsolutions.ucm.service.logical.OntologyManager;
import de.qsolutions.ucm.service.logical.QueryEvaluator;
import de.qsolutions.ucm.service.logical.QueryEvaluator.TruthValue;
import de.qsolutions.ucm.service.logical.UserQuery;
import de.qsolutions.util.MultiValueMap;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

@Component
public class DefaultContextQueryService implements ContextQueryService
{
	protected ContextFactService internalService;
	protected ConflictResolutionStrategy conflictStrategy;
	protected OntologyManager ontologyManager;
	protected QueryEvaluator queryEvaluator;
	
	protected boolean rewrite = true; 
	
	public List<FeatureValue> getValuesForUser(FeatureValueQuery q)
	{
		FactQuery[] fqs = conflictStrategy.getFactQueries(q);
		
		List<ContextFact> l = new LinkedList<ContextFact>();

		rewriteFactQueries(fqs);
		
		for (FactQuery fq : fqs)
			l.addAll(internalService.getFacts(fq));				
		
		return conflictStrategy.filter(l,q);
	}
	
	protected void rewriteFactQueries(FactQuery[] qs)
	{
		for (FactQuery q : qs)
		{
			if (q.getValueCondition() != null)
			{
				if (q.getValueCondition() instanceof CompositeCondition)
					rewriteCondition((CompositeCondition) q.getValueCondition());
				
				if (q.getValueCondition() instanceof OntologyCondition)
				{
					OntologyCondition oc = (OntologyCondition) q.getValueCondition();
					
					if ( (oc.getOp() == OntologyOperator.INSTANCE_OF) ||
							(oc.getOp() == OntologyOperator.RELATED_TO) )
						rewriteOntologyCondition(oc);
				}
			}
		}
	}
	
	protected void rewriteOntologyCondition(OntologyCondition c)
	{
		OntologyOperator op = (OntologyOperator) c.getOp();
		
		switch (op)
		{
			case INSTANCE_OF:
				List<OntologyEntity> l = ontologyManager.getInstances(OntologyEntity.fromString(c.getArg()[0]));
				c.setOp(OntologyOperator.IN);
				String[] s = new String[l.size()];
				int i = 0;
				for (OntologyEntity e : l)
					s[i++] = e.toString();
				c.setArg(s);
				break;
				
			case RELATED_TO:
				//TODO
				
		}
	}
	
	protected void rewriteCondition(CompositeCondition cc)
	{
		for (Condition c : cc)
		{
			if (c instanceof OntologyCondition)
				rewriteOntologyCondition((OntologyCondition) c);
			
			if (c instanceof CompositeCondition)
				rewriteCondition((CompositeCondition) c);
		}
	}

	
	
	public List<String> getUsers(UserQuery q)
	{
		FactQuery fq = new FactQuery();
		
		fq.setMinimumCurrentConfidence(q.getConfidence());
		List<String> l = new LinkedList<String>();
		getFeatureURIs(q.getCondition(),l);
		String[] s = new String[l.size()];
		s = l.toArray(s);
		fq.setFeatureURIs(s);
		fq.setValidPeriod(q.getValidPeriod());

		List<ContextFact> facts = internalService.getFacts(fq);
		
		MultiValueMap<String,ContextFact> userFacts = new MultiValueMap<String,ContextFact>();
		
		for (ContextFact f : facts)
			userFacts.put(f.getSubject(),f);
		
		List<String> res = new LinkedList<String>();
		for (String u : userFacts.keySet())
		{
			if (queryEvaluator.evaluateCondition(q.getCondition(), userFacts.get(u)) == TruthValue.TRUE)
				res.add(u);
		}
					
		return res;
	}
	
	protected void getFeatureURIs(Condition c, List<String> l)
	{
		if (c instanceof CompositeCondition)
		{
			for (Condition cc : ((CompositeCondition) c))
				getFeatureURIs(cc,l);			
		}
		else
		{
			FeatureValueCondition cc = (FeatureValueCondition) c;
			l.add(cc.getFeature());
		}
	}
	
	public ContextFactService getInternalService()
	{
		return internalService;
	}

	public void setInternalService(ContextFactService internalService)
	{
		this.internalService = internalService;
	}

	public ConflictResolutionStrategy getConflictStrategy()
	{
		return conflictStrategy;
	}

	public void setConflictStrategy(ConflictResolutionStrategy conflictStrategy)
	{
		this.conflictStrategy = conflictStrategy;
	}

	public OntologyManager getOntologyManager()
	{
		return ontologyManager;
	}

	public void setOntologyManager(OntologyManager ontologyManager)
	{
		this.ontologyManager = ontologyManager;
	}

	public QueryEvaluator getQueryEvaluator()
	{
		return queryEvaluator;
	}

	public void setQueryEvaluator(QueryEvaluator queryEvaluator)
	{
		this.queryEvaluator = queryEvaluator;
	}
	
}
