package de.qsolutions.ucm.service.logical.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import de.qsolutions.ucm.model.ContextFact;
import de.qsolutions.ucm.model.Feature;
import de.qsolutions.ucm.model.Mode;
import de.qsolutions.ucm.query.CompositeCondition;
import de.qsolutions.ucm.query.Condition;
import de.qsolutions.ucm.query.ConditionAtom;
import de.qsolutions.ucm.query.Connector;
import de.qsolutions.ucm.service.internal.SchemaManager;
import de.qsolutions.ucm.service.logical.FeatureValueCondition;
import de.qsolutions.ucm.service.logical.QueryEvaluator;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

@Component
public class DefaultQueryEvaluator implements QueryEvaluator
{
	protected SchemaManager schemaManager;

	public TruthValue evaluateCondition(Condition c, List<ContextFact> facts)
	{
		if (c instanceof CompositeCondition)
			return evaluateComposite((CompositeCondition)c,facts);
		else
			return evaluateAtomic((FeatureValueCondition)c,facts);
	}
	
	protected TruthValue evaluateComposite(CompositeCondition c, List<ContextFact> facts)
	{
		if (c.getConnector() == Connector.NOT)
		{
			Condition cc = c.iterator().next();
			
			TruthValue v;
			
			if (cc instanceof CompositeCondition)
				v = evaluateComposite((CompositeCondition) cc,facts);
			else
				v = evaluateAtomic((FeatureValueCondition) cc,facts);

			return combine(Connector.NOT,v,null);
		}

		TruthValue v;
		
		if (c.getConnector() == Connector.AND)
			v = TruthValue.TRUE;
		else
			v = TruthValue.FALSE;
		
		for (Condition cc: c)
		{
			TruthValue w;
			if (cc instanceof CompositeCondition)
				w = evaluateComposite((CompositeCondition) cc,facts);
			else
				w = evaluateAtomic((FeatureValueCondition) cc,facts);
			v = combine(c.getConnector(),v,w);			
		}		
		
		return v;
	}
	
	protected TruthValue evaluateAtomic(FeatureValueCondition fc, List<ContextFact> facts)
	{
		ConditionAtom a = fc.getCondition();
		Feature ft = schemaManager.getFeatureForURI(fc.getFeature());
		
		int m_plus_true = 0;
		int m_plus_false = 0;
		int m_minus_true = 0;
		int m_minus_false = 0;
		
		for (ContextFact f : facts)
		{
			if (f.getFeature().isSubFeatureOfOrEqual(ft))
			{
				if (a.evaluate(f.getValue()))
				{
					if (f.getMode() == Mode.POSITIVE)
						m_plus_true++;
					else
						m_minus_true++;				
				}
				else
				{
					if (f.getMode() == Mode.POSITIVE)
						m_plus_false++;
					else
						m_minus_false++;				
				}	
			}
		}
		
		if (ft.getCardinality() == 1)
		{
			if (m_plus_true > 0)
			{
				if ((m_plus_false == 0) && (m_minus_true == 0))
					return TruthValue.TRUE;
				else
					return TruthValue.CONTRADICTORY;
			}
			else
			{
				if ( ( m_plus_false == 0) && (m_minus_true == 0))
					return TruthValue.UNKNOWN;
				else
					return TruthValue.FALSE;
			}
		}
		else
		{
			if (m_plus_true > 0)
			{
				if (m_minus_true == 0)				
					return TruthValue.TRUE;
				else
					return TruthValue.CONTRADICTORY;			
			}
			else
			{
				if (m_minus_true == 0)
					return TruthValue.UNKNOWN;
				else
					return TruthValue.FALSE;
					
			}
		}
	}
	
	
	protected TruthValue combine(Connector c, TruthValue v, TruthValue w)
	{
		switch (c)
		{
			case AND:
				switch (v)
				{
					case TRUE:
						return w;
					case FALSE:
						return TruthValue.FALSE;
					case UNKNOWN:						
						switch (w)
						{
							case TRUE:
							case CONTRADICTORY:
								return TruthValue.UNKNOWN;

							case FALSE:
							case UNKNOWN:
								return TruthValue.FALSE;
						}
					case CONTRADICTORY:
						switch (w)
						{
							case TRUE:
							case CONTRADICTORY:
								return TruthValue.CONTRADICTORY;

							case FALSE:
							case UNKNOWN:
								return TruthValue.FALSE;
						}
				}
				
			case OR:
				switch (v)
				{
					case TRUE:
						return TruthValue.TRUE;
					case FALSE:
						return w;
					case UNKNOWN:
						switch (w)
						{
							case TRUE:
							case CONTRADICTORY:
								return TruthValue.TRUE;

							case FALSE:
							case UNKNOWN:
								return TruthValue.UNKNOWN;
						}
					case CONTRADICTORY:
						switch (w)
						{
							case TRUE:
							case UNKNOWN:
								return TruthValue.TRUE;

							case CONTRADICTORY:
							case FALSE:
								return TruthValue.CONTRADICTORY;
						}						
				}
				
			
			case NOT:
				switch (v)
				{
					case TRUE:
						return TruthValue.FALSE;
					case FALSE:
						return TruthValue.TRUE;
					case UNKNOWN:
						return TruthValue.CONTRADICTORY;
					case CONTRADICTORY:
						return TruthValue.UNKNOWN;
				}
		}
		
		return v;
	}

	public SchemaManager getSchemamanager()
	{
		return schemaManager;
	}

	public void setSchemamanager(SchemaManager schemamanager)
	{
		this.schemaManager = schemamanager;
	}

}
