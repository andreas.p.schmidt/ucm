package de.qsolutions.ucm.service.logical;

import java.util.List;

import de.qsolutions.ucm.model.OntologyEntity;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface OntologyManager
{
	public List<OntologyEntity> getInstances(OntologyEntity concept);
	public List<OntologyEntity> getQueryResult(String queryResult);
}
