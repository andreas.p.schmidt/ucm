package de.qsolutions.ucm.service.logical;

import de.qsolutions.ucm.query.Condition;
import de.qsolutions.ucm.query.ConditionAtom;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class FeatureValueCondition implements Condition
{
	protected String feature;
	protected ConditionAtom condition;
	public String getFeature()
	{
		return feature;
	}
	public void setFeature(String feature)
	{
		this.feature = feature;
	}
	public ConditionAtom getCondition()
	{
		return condition;
	}
	public void setCondition(ConditionAtom condition)
	{
		this.condition = condition;
	}
}
