package de.qsolutions.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class MultiValueMap<K,T>
{
	protected HashMap<K,List<T>> map;

	public int size() 
	{
		return map.size();
	}

	public boolean isEmpty() 
	{
		return map.isEmpty();
	}

	public boolean containsKey(Object key) 
	{
		return map.containsKey(key);
	}

	public boolean containsValue(Object arg0) 
	{
		for (List<T> l : map.values())
		{
			 if (l.contains(arg0))
			    return true;
		}
		return false;
	}

	public List<T> get(Object arg0) 
	{
		return map.get(arg0);
	}

	public T put(K arg0, T arg1) 
	{
		if (!map.containsKey(arg0))
		{
			map.put(arg0,new LinkedList<T>());
		}
		
		map.get(arg0).add(arg1);
		
		return arg1;
	}

	public T remove(K k, T t) 
	{
		if (map.containsKey(k))
		{
			map.get(k).remove(t);
		}
		
		return t;
	}
	
	public void clear() 
	{
		map.clear();
	}

	public Set<K> keySet() 
	{
		return map.keySet();
	}

	public Collection<T> values() 
	{
		LinkedList<T> res = new LinkedList<T>();
		
		for (List<T> l : map.values())
		{
			res.addAll(l);
		}
		
		return res;
	}
}
