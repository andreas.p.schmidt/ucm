/*
 * ============================================================================
 *                   The Apache Software License, Version 1.1
 * ============================================================================
 *
 *    Copyright (C) 1999 The Apache Software Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modifica-
 * tion, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by the  Apache Software Foundation  (http://www.apache.org/)."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "log4j" and  "Apache Software Foundation"  must not be used to
 *    endorse  or promote  products derived  from this  software without  prior
 *    written permission. For written permission, please contact
 *    apache@apache.org.
 *
 * 5. Products  derived from this software may not  be called "Apache", nor may
 *    "Apache" appear  in their name,  without prior written permission  of the
 *    Apache Software Foundation.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  THE
 * APACHE SOFTWARE  FOUNDATION  OR ITS CONTRIBUTORS  BE LIABLE FOR  ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL,  EXEMPLARY, OR CONSEQUENTIAL  DAMAGES (INCLU-
 * DING, BUT NOT LIMITED TO, PROCUREMENT  OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR  PROFITS; OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON
 * ANY  THEORY OF LIABILITY,  WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT
 * (INCLUDING  NEGLIGENCE OR  OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software  consists of voluntary contributions made  by many individuals
 * on  behalf of the Apache Software  Foundation.  For more  information on the
 * Apache Software Foundation, please see <http://www.apache.org/>.
 *
 */

package org.apache.log4j.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


/**
 * This class maintains list of contexts in {@link ServletContextLogAppender}.
 * <p>A container-managed registration of contexts would obviate the need
 * for this class, but since there is no container-management at this time,
 * this class is the only way to register the servlet context for your
 * application in order for the <code>ServletContextLogAppender</code>
 * to be able to log to your application's context log.</p>
 * <p>To use this class, the following must be added to the web.xml (and
 * make sure that it is added before the
 * <code>{@link InitContextListener}</code> if you use that class for Log4j
 * initialization)...
 * <pre>
 * &lt;listener&gt;
 *   &lt;listener-class&gt;
 *   org.apache.log4j.servlet.ServletContextLogAppenderListener
 *   &lt;/listener-class&gt;
 * &lt;/listener&gt;
 * </pre></p>
 * <p>See the <code>ServletContextLogAppender</code> for further required
 * configuration information.</p>
 *
 * @author Aleksei Valikov
 * @author <a href="mailto:hoju@visi.com">Jacob Kjome</a>
 * @since  1.3
 */
public class ServletContextLogAppenderListener
  implements ServletContextListener {
  /**
   * Receives a notification of the context initialization event.
   * @param event context event.
   */
  public void contextInitialized(final ServletContextEvent event) {
    // Add context to map of servlet contexts
    final ServletContext servletContext = event.getServletContext();
    ServletContextLogAppender.servletContexts().put(
      contextPath(servletContext), servletContext);
  }

  /**
   * Receives a notification of the context destruction event.
   * @param event context event.
   */
  public void contextDestroyed(final ServletContextEvent event) {
    // Removes context from the map of servlet contexts
    final ServletContext servletContext = event.getServletContext();
    ServletContextLogAppender.servletContexts().remove(
      contextPath(servletContext));
  }

  /**
   * utility method to obtain the servlet context path such as
   * &quot;/MyContext&quot;.
   *
   * @param servletContext a servlet context object
   * @return a string representing the servlet context path
   */
  private static String contextPath(ServletContext servletContext) {
    String contextPath = "";

    try {
      String path = servletContext.getResource("/").getPath();

      //first remove trailing slash, then take what's left over
      //which should be the context path such as "/MyContext"
      contextPath = path.substring(0, path.lastIndexOf("/"));
      contextPath = contextPath.substring(contextPath.lastIndexOf("/"));
    } catch (Exception e) {
      ;
    }

    return contextPath;
  }
}
