/*
 * ============================================================================
 *                   The Apache Software License, Version 1.1
 * ============================================================================
 *
 *    Copyright (C) 1999 The Apache Software Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modifica-
 * tion, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by the  Apache Software Foundation  (http://www.apache.org/)."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "log4j" and  "Apache Software Foundation"  must not be used to
 *    endorse  or promote  products derived  from this  software without  prior
 *    written permission. For written permission, please contact
 *    apache@apache.org.
 *
 * 5. Products  derived from this software may not  be called "Apache", nor may
 *    "Apache" appear  in their name,  without prior written permission  of the
 *    Apache Software Foundation.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  THE
 * APACHE SOFTWARE  FOUNDATION  OR ITS CONTRIBUTORS  BE LIABLE FOR  ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL,  EXEMPLARY, OR CONSEQUENTIAL  DAMAGES (INCLU-
 * DING, BUT NOT LIMITED TO, PROCUREMENT  OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR  PROFITS; OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON
 * ANY  THEORY OF LIABILITY,  WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT
 * (INCLUDING  NEGLIGENCE OR  OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software  consists of voluntary contributions made  by many individuals
 * on  behalf of the Apache Software  Foundation.  For more  information on the
 * Apache Software Foundation, please see <http://www.apache.org/>.
 *
 */

package org.apache.log4j.servlet;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.spi.RepositorySelector;
import org.apache.log4j.xml.DOMConfigurator;


/**
 * Used to initialize and cleanup Log4j in a servlet environment.
 * <p>See {@link InitContextListener} for web.xml configuration instructions.</p>
 * <p>Besides basic webapp lifecycle control when used with InitContextListener
 * and {@link InitServlet}, this class can be used by any class at runtime to
 * control initialization and shutdown of Log4j loggers and appenders.</p>
 *
 * @author <a href="mailto:hoju@visi.com">Jacob Kjome</a>
 * @since  1.3
 */
public class InitShutdownController {
  /**
   * preferred repository selector config param.
   * Maps to web.xml &lt;context-param&gt; with
   * &lt;param-name&gt;log4j-selector&lt;/param-name&gt;
   */
  public static final String PARAM_LOG4J_PREF_SELECTOR = "log4j-selector";

  /**
   * relative path to config file within current webapp config param.
   * Maps to web.xml &lt;context-param&gt; with
   * &lt;param-name&gt;log4j-config&lt;/param-name&gt;
   */
  public static final String PARAM_LOG4J_CONFIG_PATH = "log4j-config";

  /**
   * config file re-reading specified in milliseconds config param.
   * Maps to web.xml &lt;context-param&gt; with
   * &lt;param-name&gt;log4j-cron&lt;/param-name&gt;
   */
  public static final String PARAM_LOG4J_WATCH_INTERVAL = "log4j-cron";

  /**
   * path to be read from a log4j xml config file as a system property
   * config param. Maps to web.xml &lt;context-param&gt; with
   * &lt;param-name&gt;log4j-log-home&lt;/param-name&gt;
   */
  public static final String PARAM_LOG4J_LOG_HOME = "log4j-log-home";

  /**
   * system property to be used in a log4j xml config file config param.
   * Maps to web.xml &lt;context-param&gt; with
   * &lt;param-name&gt;log4j-sysprop-name&lt;/param-name&gt;
   */
  public static final String PARAM_LOG4J_SYSPROP_NAME = "log4j-sysprop-name";

  /**
   * default path to write log files if using a file appender
   */
  private static final String DEFAULT_LOG_HOME =
    "WEB-INF" + File.separator + "logs";

  /**
   * Log4j specific cleanup.  Shuts down all loggers and appenders.
   *
   * @param context the current servlet context
   */
  public static void shutdownLog4j(final ServletContext context) {
    //shutdown this webapp's logger repository
    context.log(
      "Cleaning up Log4j resources for context: "
      + context.getServletContextName() + "...");
    context.log("Shutting down all loggers and appenders...");
    org.apache.log4j.LogManager.shutdown();
    context.log("Log4j cleaned up.");
  }

  /**
   * Log4j specific initialization.  Sets up log4j for the current
   * servlet context. Installs a custom repository selector if one hasn't
   * already been installed.
   *
   * @param context the current servlet context
   */
  public static void initializeLog4j(final ServletContext context) {
    String configPath = context.getInitParameter(PARAM_LOG4J_CONFIG_PATH);

    // if the log4j-config parameter is not set, then no point in trying
    if (configPath != null) {
      if (configPath.startsWith("/")) {
        configPath = (configPath.length() > 1) ? configPath.substring(1) : "";
      }

      // if the configPath is an empty string, then no point in trying
      if (configPath.length() >= 1) {
        // set up log path System property
        String logHome = context.getInitParameter(PARAM_LOG4J_LOG_HOME);

        if (logHome != null) {
          // set up custom log path system property
          setFileAppenderSystemProperty(logHome, context);
        }

        boolean isXMLConfigFile = (configPath.endsWith(".xml")) ? true : false;
        String contextPath = context.getRealPath("/");

        if (contextPath != null) {
          // The webapp is deployed directly off the filesystem,
          // not from a .war file so we *can* do File IO.
          // This means we can use configureAndWatch() to re-read
          // the the config file at defined intervals.
          // Now let's check if the given configPath actually exists.
          if (logHome == null) {
            // no log path specified in web.xml. Setting to default
            logHome = contextPath + DEFAULT_LOG_HOME;
            setFileAppenderSystemProperty(logHome, context);
          }

          String systemConfigPath =
            configPath.replace('/', File.separatorChar);
          File log4jFile = new File(contextPath + systemConfigPath);

          if (log4jFile.canRead()) {
            log4jFile = null;

            String timerInterval =
              context.getInitParameter(PARAM_LOG4J_WATCH_INTERVAL);
            long timerIntervalVal = 0L;

            if (timerInterval != null) {
              try {
                timerIntervalVal = Integer.valueOf(timerInterval).longValue();
              } catch (NumberFormatException nfe) {
                //ignore...we just won't use configureAndWatch if there is no
                //valid int
                ;
              }
            }

            setSelector(context);
            context.log(
              "Configuring Log4j from File: " + contextPath + systemConfigPath);

            if (timerIntervalVal > 0) {
              context.log(
                "Configuring Log4j with watch interval: " + timerIntervalVal
                + "ms");

              if (isXMLConfigFile) {
                DOMConfigurator.configureAndWatch(
                  contextPath + systemConfigPath, timerIntervalVal);
              } else {
                PropertyConfigurator.configureAndWatch(
                  contextPath + systemConfigPath, timerIntervalVal);
              }
            } else {
              if (isXMLConfigFile) {
                DOMConfigurator.configure(contextPath + systemConfigPath);
              } else {
                PropertyConfigurator.configure(contextPath + systemConfigPath);
              }
            }
          } else {
            //The given configPath does not exist.  So, let's just let Log4j
            //look for the default files (log4j.properties or log4j.xml) on
            //its own.
            displayConfigNotFoundMessage();
          }

          //end log4jFile.canRead() check
        } else {
          //The webapp is deployed from a .war file, not directly
          //off the file system so we *cannot* do File IO.
          //Note that we *won't* be able to use configureAndWatch() here
          //because that requires an absolute system file path.
          //Now let's check if the given configPath actually exists.
          URL log4jURL = null;

          try {
            log4jURL = context.getResource("/" + configPath);
          } catch (MalformedURLException murle) {
            //ignore...we check for null later
            ;
          }

          if (log4jURL != null) {
            setSelector(context);
            context.log("Configuring Log4j from URL at path: /" + configPath);

            if (isXMLConfigFile) {
              try {
                DOMConfigurator.configure(log4jURL);

                //catch (javax.xml.parsers.FactoryConfigurationError fce) {}
              } catch (Exception e) {
                //report errors to server logs
                LogLog.error(e.getMessage());
              }
            } else {
              Properties log4jProps = new Properties();

              try {
                log4jProps.load(log4jURL.openStream());
                PropertyConfigurator.configure(log4jProps);

                //catch (java.io.IOException ioe) {}
              } catch (Exception e) {
                //report errors to server logs
                LogLog.error(e.getMessage());
              }
            }
          } else {
            //The given configPath does not exist.  So, let's just let Log4j
            //look for the default files (log4j.properties or log4j.xml) on
            //its own.
            displayConfigNotFoundMessage();
          }

          //end log4jURL null check
        }

        //end contextPath null check
      } else {
        LogLog.error("Zero length Log4j config file path given.");
        displayConfigNotFoundMessage();
      }

      //end configPath length check
    } else {
      LogLog.error("Missing log4j-config servlet parameter missing.");
      displayConfigNotFoundMessage();
    }

    //end configPath null check
  }

  /**
   * standard configuration not found message
   */
  private static void displayConfigNotFoundMessage() {
    LogLog.warn(
      "No Log4j configuration file found at given path. "
      + "Falling back to Log4j auto-configuration.");
  }

  /**
   * sets the [webapp].log.home system property for use with a file appender.
   *
   * @param logHome the path to a logging directory
   * @param context the current servlet context
   */
  private static void setFileAppenderSystemProperty(
    final String logHome, final ServletContext context) {
    String logHomePropName = null;
    String customPropName = context.getInitParameter(PARAM_LOG4J_SYSPROP_NAME);

    if (customPropName != null) {
      logHomePropName = customPropName;
    } else {
      File logHomeDir = new File(logHome);

      if (logHomeDir.exists() || logHomeDir.mkdirs()) {
        /*String tempdir =
          "" + context.getAttribute("javax.servlet.context.tempdir");
        int lastSlash = tempdir.lastIndexOf(File.separator);

        if ((tempdir.length() - 1) > lastSlash) {
          logHomePropName = tempdir.substring(lastSlash + 1) + ".log.home";
        }*/
        String contextPath = "";

        try {
          //use a more standard way to obtain the context path name
          //which should work across all servers.  The tmpdir technique
          //(above) depends upon the naming scheme that Tomcat uses.
          String path = context.getResource("/").getPath();

          //first remove trailing slash, then take what's left over
          //which should be the context path less the preceeding
          //slash such as "MyContext"
          contextPath = path.substring(0, path.lastIndexOf("/"));
          contextPath =
            contextPath.substring(contextPath.lastIndexOf("/") + 1);
        } catch (Exception e) {
          ;
        }

        logHomePropName = contextPath + ".log.home";
      }
    }

    if (logHomePropName != null) {
      context.log(
        "Setting system property [ " + logHomePropName + " ] to [ " + logHome
        + " ]");
      System.setProperty(logHomePropName, logHome);
    } else {
      context.log(
        "Unable to derive log4j system property name. Consider setting the "
        + "\"log4j-sysprop-name\" context parameter. No system property set.");
    }
  }

  /**
   * Do idempotent initialization of the the logger repository.  Only one
   * repository selector may be set during runtime of log4j.  Therefore, this
   * method is only a guarantee that a repository selector will have been set.
   * It does not guarantee that your preferred selector will be used.  If
   * some other code sets the selector first, that is the selector that all
   * applications using Log4j will use, assuming log4j is running in a shared
   * class loader.
   *
   * @param context the current servlet context
   */
  private static void setSelector(final ServletContext context) {
    String selector = context.getInitParameter(PARAM_LOG4J_PREF_SELECTOR);

    if (selector == null) {
      LogLog.warn(
        "No preferred selector supplied. Using default repository selector...");

      return;
    }

    try {
      Object guard = new Object();

      Class clazz =
        Class.forName(
          selector, true, Thread.currentThread().getContextClassLoader());
      LogManager.setRepositorySelector(
        (RepositorySelector) clazz.newInstance(), guard);
    } catch (ClassNotFoundException cnfe) {
      LogLog.warn(
        "Preferred selector not found. Using default repository selector...");
    } catch (InstantiationException ie) {
      LogLog.warn(
        "Error in instantiation of preferred selector. Using default "
        + "repository selector...");
    } catch (IllegalAccessException iae) {
      LogLog.warn(
        "Unable to access preferred selector. Using default repository "
        + "selector...");
    } catch (IllegalArgumentException iae) {
      LogLog.warn(
        "Preferred repository selector not installed because one has already "
        + "exists.  No problem, using existing selector...");
    }
  }
}
