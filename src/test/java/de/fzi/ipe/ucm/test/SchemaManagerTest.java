package de.fzi.ipe.ucm.test;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.qsolutions.ucm.model.Feature;
import de.qsolutions.ucm.model.Type;
import de.qsolutions.ucm.service.internal.SchemaManager;

import static org.junit.Assert.fail;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( { "/oracle-config.xml" })
public class SchemaManagerTest
{
	@Autowired
	protected SchemaManager sm;
	
	@BeforeClass
	protected void setUp() throws Exception
	{
		BasicConfigurator.configure();
	}
	
	@Test
	public void testSchemaManager()
	{
		final String featureURI = "http://www.fzi.de/ipe/ucm/test";
		
		sm.addFeature(featureURI,Type.STRING,1,null,null,"exp",new double[] { 10 });	
		sm.addFeature(featureURI + "2",Type.NUMBER,10,null,Collections.singletonList(featureURI),"exp",new double[] { 1,2,3} );
		
		
		Feature f = sm.getFeatureForURI(featureURI);
		
		if (f == null)
		{
			fail("Feature not found.");
		}
		
		List<Feature> l = sm.getParentFeatures(f);
		
		for (Feature ff : l)
			System.out.println(ff.getURI());
	
		
		l = sm.getRootFeatures();
		for (Feature ff : l)
			System.out.println(ff.getURI());

		sm.deleteFeature(featureURI);
		sm.deleteFeature(featureURI + "2");
	}
	

}
