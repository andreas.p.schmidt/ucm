package de.fzi.ipe.ucm.test;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.qsolutions.ucm.model.Mode;
import de.qsolutions.ucm.model.Type;
import de.qsolutions.ucm.service.internal.AddFeatureValue;
import de.qsolutions.ucm.service.internal.ContextStorage;
import de.qsolutions.ucm.service.internal.SchemaManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( { "/oracle-config.xml" })
public class UpdateTest 
{
	@Autowired
	protected ContextStorage update;
	@Autowired
	protected SchemaManager sm;
	protected final String featureURI = "http://www.fzi.de/ipe/ucm/test";
	
	@BeforeClass
	protected void setUp() throws Exception
	{
		BasicConfigurator.configure();
		
		assertTrue(sm.addFeature(featureURI,Type.STRING,1,null,null,"exp",new double[] { 10,5 }));	
	}
	
	@AfterClass
	protected void tearDown()
	{
		sm.deleteFeature(featureURI);
		update.clearFacts();
	}
	
	@Test
	public void testUpdate()
	{		
		AddFeatureValue v = new AddFeatureValue();
		v.setConfidence(0.8);
		v.setFeatureURI(featureURI);
		v.setOperator(Mode.POSITIVE);
		v.setSource("JUnit");
		v.setTransactionTime(new Date());
		v.setValidFrom(new Date());
		v.setValue("TestString");
		v.setUser("http://www.fzi.de/ipe/aschmidt");
		
		List<AddFeatureValue> l = new LinkedList<AddFeatureValue>();
		l.add(v);
		
		assertTrue(update.addFacts(l));
	}
}
